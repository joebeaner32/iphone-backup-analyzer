# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about_window.ui'
#
# Created: Tue Feb 19 10:20:21 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AboutWindow(object):
    def setupUi(self, AboutWindow):
        AboutWindow.setObjectName("AboutWindow")
        AboutWindow.resize(393, 359)
        self.verticalLayout = QtGui.QVBoxLayout(AboutWindow)
        self.verticalLayout.setObjectName("verticalLayout")
        self.about_title = QtGui.QLabel(AboutWindow)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setWeight(75)
        font.setBold(True)
        self.about_title.setFont(font)
        self.about_title.setTextFormat(QtCore.Qt.RichText)
        self.about_title.setObjectName("about_title")
        self.verticalLayout.addWidget(self.about_title)
        self.label = QtGui.QLabel(AboutWindow)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.about_version = QtGui.QLabel(AboutWindow)
        self.about_version.setObjectName("about_version")
        self.verticalLayout.addWidget(self.about_version)
        self.label_2 = QtGui.QLabel(AboutWindow)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtGui.QLabel(AboutWindow)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.plainTextEdit = QtGui.QPlainTextEdit(AboutWindow)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.verticalLayout.addWidget(self.plainTextEdit)
        self.label_4 = QtGui.QLabel(AboutWindow)
        self.label_4.setTextFormat(QtCore.Qt.RichText)
        self.label_4.setObjectName("label_4")
        self.verticalLayout.addWidget(self.label_4)

        self.retranslateUi(AboutWindow)
        QtCore.QMetaObject.connectSlotsByName(AboutWindow)

    def retranslateUi(self, AboutWindow):
        AboutWindow.setWindowTitle(QtGui.QApplication.translate("AboutWindow", "About iPBA2", None, QtGui.QApplication.UnicodeUTF8))
        self.about_title.setText(QtGui.QApplication.translate("AboutWindow", "iP Backup Analyzer 2", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("AboutWindow", "Open Source tool for iOS backup data analysis", None, QtGui.QApplication.UnicodeUTF8))
        self.about_version.setText(QtGui.QApplication.translate("AboutWindow", "Version:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("AboutWindow", "<html><head/><body><p>Copyright (C) 2013 Mario Piccinelli &lt;<a href=\"mailto:mario.piccinelli@gmail.com\"><span style=\" text-decoration: underline; color:#0000ff;\">mario.piccinelli@gmail.com</span></a>&gt;</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("AboutWindow", "Released under MIT License:", None, QtGui.QApplication.UnicodeUTF8))
        self.plainTextEdit.setPlainText(QtGui.QApplication.translate("AboutWindow", "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n"
"\n"
"The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n"
"\n"
"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("AboutWindow", "<html><head/><body><p>see: <a href=\"http://www.ipbackupanalyzer.com\"><span style=\" text-decoration: underline; color:#0000ff;\">http://www.ipbackupanalyzer.com</span></a></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

