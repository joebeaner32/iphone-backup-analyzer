# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sms_ui.ui'
#
# Created: Thu Feb 21 12:01:59 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_SMS(object):
    def setupUi(self, SMS):
        SMS.setObjectName("SMS")
        SMS.resize(623, 437)
        self.horizontalLayout = QtGui.QHBoxLayout(SMS)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.dataText = QtGui.QTextEdit(SMS)
        self.dataText.setMaximumSize(QtCore.QSize(200, 120))
        self.dataText.setReadOnly(True)
        self.dataText.setObjectName("dataText")
        self.verticalLayout_2.addWidget(self.dataText)
        self.threadsTree = QtGui.QTreeWidget(SMS)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.threadsTree.sizePolicy().hasHeightForWidth())
        self.threadsTree.setSizePolicy(sizePolicy)
        self.threadsTree.setMaximumSize(QtCore.QSize(200, 16777215))
        self.threadsTree.setSizeIncrement(QtCore.QSize(0, 0))
        self.threadsTree.setBaseSize(QtCore.QSize(0, 0))
        self.threadsTree.setObjectName("threadsTree")
        self.verticalLayout_2.addWidget(self.threadsTree)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.threadLabel = QtGui.QLabel(SMS)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.threadLabel.sizePolicy().hasHeightForWidth())
        self.threadLabel.setSizePolicy(sizePolicy)
        self.threadLabel.setFrameShape(QtGui.QFrame.Panel)
        self.threadLabel.setFrameShadow(QtGui.QFrame.Raised)
        self.threadLabel.setText("")
        self.threadLabel.setObjectName("threadLabel")
        self.verticalLayout.addWidget(self.threadLabel)
        self.messageTable = QtGui.QTableWidget(SMS)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.messageTable.sizePolicy().hasHeightForWidth())
        self.messageTable.setSizePolicy(sizePolicy)
        self.messageTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.messageTable.setTextElideMode(QtCore.Qt.ElideNone)
        self.messageTable.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.messageTable.setRowCount(0)
        self.messageTable.setColumnCount(0)
        self.messageTable.setObjectName("messageTable")
        self.messageTable.setColumnCount(0)
        self.messageTable.setRowCount(0)
        self.verticalLayout.addWidget(self.messageTable)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.setStretch(1, 1)

        self.retranslateUi(SMS)
        QtCore.QMetaObject.connectSlotsByName(SMS)

    def retranslateUi(self, SMS):
        SMS.setWindowTitle(QtGui.QApplication.translate("SMS", "Messages Browser", None, QtGui.QApplication.UnicodeUTF8))
        self.threadsTree.headerItem().setText(0, QtGui.QApplication.translate("SMS", "id", None, QtGui.QApplication.UnicodeUTF8))
        self.threadsTree.headerItem().setText(1, QtGui.QApplication.translate("SMS", "Chat", None, QtGui.QApplication.UnicodeUTF8))

